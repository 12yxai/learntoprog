//Programmers are usually asked for a variation of the following during a 
//interview to see IF they are even able to program

//Problem: Write a program that prints the numbers 1 to 100 (or some other value)
//saying FIZZ if the number sis divisible by 3 and BUZZ if it is divisible by 5
//the result is for numbers like 15 FIZZBUZZ is displayed

//Example: 1 2 FIZZ 4 BUZZ FIZZ 7 8 FIZZ BUZZ .....

//Note: We need the remainder operator for this
//1. We could write it out ourselves but that defeats the purpose
//We can use while loops!
var whileCounter = 1;

//2.We need some way of displaying BRANCHING possibilities
//We can use IF ELSE chains!

while (whileCounter < 100) {
    if ((whileCounter % 3) === 0 && (whileCounter % 5) === 0)
        console.log("FIZZBUZZ");
    else if ((whileCounter % 3) === 0)
        console.log("FIZZ");
    else if ((whileCounter % 5) === 0)
        console.log("BUZZ");
    else
        console.log(whileCounter);
    whileCounter += 1;
}