//Booleans

//Boolean values are really cool as they enable you to evaluate whether
//expressions are true or false. However they can get complicated so lets go 
//through their capabilities.

//What happens when you want to know if a variable is within a certain range?
//Ex: Is X less than 5?

//These types of expressions can be evaluated!
var x = -3;
x < 5;

//These expressions evaluate to booleans!
var test = x < 5;
console.log(test + "");

//What types of evaluation can we do?
//Is x LESS than 5                  x < 5
//IS x GREATER than 5               x > 5
//IS x GREATER than OR EQUAL to 5   x >= 5
//IS x LESS than OR EQUAL to 5      x <= 5
//IS x EQUAL to 5                   x === 5
//IS X NOT EQUAL to 5               x !== 5

//HOLD ON! Why does equal to use === ?????
//Remember = is the ASSIGNMENT operator! That doesn't change ever. We need another operator to do that.
//Then why not ==? Javascript does have == but it acts in funny ways, as it performs a type conversion!
//Type conversions mean that if you compare 5 as a NUMBER and "5" as a STRING, they are equal.
//But let's say you want to make sure that you ONLY get the right answer if the types are the SAME:
//  ie. If you compare "5" and 5 they would NOT be equal.

//Check it out
var test1 = 5;
var test2 = "5";

console.log("_________________________________");
console.log("Using the double equals:");
console.log(test1 == test2);
console.log("Using the triple equals:");
console.log(test1 === test2);
console.log("What happens when we use the assignment operator:");
console.log(test1 = test2);
