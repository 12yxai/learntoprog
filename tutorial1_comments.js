//Comments

//Comments are how a programmer can display meaningful text that describes 
//their code. It is highly important to document code that would otherwise
//be unreadable.

//Example

//This code prints hello 5 times
for (var i = 0; i < 5; i++) {
    console.log("HELLO");
}

//The meaning isn't exactly clear from the code, unless you have been 
//programming for a while. 

//How to use comments:
//There are two types of comments

//Single Line
console.log("Which are special because");//They can be used on the same line as code
//However any code after the double // isn't executed, the computer ignores it

//console.log("SEE WHAT I MEAN? THIS WON'T APPEAR IN OUTPUT");
console.log("But this code will appear....");
/*The other type of comment is a multiline comment and like
the single line comments any code in here:
console.log("is ignored as well");*/

//Comments are programmer's ways of telling other what is going on in their code
//Sometimes many programmers will be looking at the same code and without
//comments it can be difficult to near impossible to get the exact idea the 
//other programmer had in mind! Also it helps to comment code you use only for
//yourself, for those situations where you need to look at code from a few years
//ago!

