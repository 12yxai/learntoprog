//Variables

//Variables allow you to chnage a value in your code and get different code
//35 + 4  will always = 39
console.log(35 + 4);
//Variables allow you to store any value
//For example x + 34 = ???
//It depends on what we make x equal!

//Syntax for declaring variables: the var keyword!
var x;
//In order to use a variable we need to set it equal to something
//Assignment happens from left to right
//Ex x = 38  not 38 = x
x = 38;

//We can combine these two expressions into one line
var y = 35;

//Make sure you are aware of the right to left assignment!
//y = x is not the same as x = y!!!!
// = is known as the ASSIGNMENT operator, not equality. 

//Question: What is the displayed result of the following expression?

//var z = 99;
//console.log(35 + 5 + x);

//Run the code to find out, but make sure to un-comment(remove the // in order) 
//to make it work!

//Naming your variables:
//There are some rules in order to name variables
//You can't separate the name using spaces
//var spaces aren't allowed = 5;

//You also can't use special characters like $,# or ! as they all have other uses
//var !SO&you--can't***do@this = 99;

//The only special character you can use is are letters, numbers and _
var use_the_special_char_like_a_space = 50;
console.log(use_the_special_char_like_a_space);
var thisislegaltoo55;

//I don't recommend this one though.....
var _ = "g";
console.log(_);