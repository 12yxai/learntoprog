//Branches
//What if there are multiple cases in which we need to address?
//Example: What if the door needs an alarm if the ATM card number is right but the name
//is not, what about he other way?
var personName = "Alex";
var personID = 55667788;

//We can chain else and if
if (personName === "Alex" && personID === 55667788)
    console.log("Sending out $20");
else if (personName !== "Alex" && personID === 55667788)
    console.log("SOUNDING ALARM");
else if (personID === "Alex" &&  personID !== 55667788)
    console.log("Sounded the Alarm");
else//If both are not equal
    console.log("Rejection of ID and Name");
    
//If elses can be chained infinitely
//However what if we have a simple case where we just want different outputs if
//a number is different
//i.e
console.log("----------------------------------------");
var someNumb = 3;
if (someNumb === 2)
    console.log("Two");
else if (someNumb === 3)
    console.log("Three");
else if (someNumb === 4)
    console.log("Four");
else
    console.log("Not listed");
    
console.log("---------------------------------------");
//This can be done with a switch (like a light switch except with more than two options)
switch(someNumb) {//Note that the cases need to be inside a block together
    case 2:
        console.log("Two");
        break;
    case 3:
        console.log("Three");
        break;
    case 4:
        console.log("Four");
        break;
    default:
        console.log("Not Listed");
}

//It works for characters too!
//Watch what happens if I don't put a break though, it 'FALLS THROUGH' meaning it keeps executing downward
console.log("-----------------------------------------------");
var someChar = 'a';
switch(someChar) {
    case 'a':
        console.log(3 + 4);
    case 'f':
        console.log(4 + 2);
        console.log(4 + 7);
        //Unlike if statements cases do not need to be grouped in order to be executed
        break;
    default:
        console.log("No Match");
