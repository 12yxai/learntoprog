//Typing

//Javascript is what is known as a WEAKLY typed language. In other words a 
//variable is allowed to hold an integer, a string, a character, etc without caring

//In other languages using a variable that stores integers to store a string is NOT allowed
//It is used to prevent programmers and their programs from behaving in ways that they didn't intend.

//For example imagine I want to do a calculation say hi to the user and then output the result of the calculation:

var x = 5;
x = "HI GUYS!";//Storing our expression
console.log(x);//Saying Hi to the user
var output = x + 7;//We want this to equal 12
console.log(output);

///Wait a second......
//We wanted it to output 12 not HI GUYS!7 what happened there.......
//This is a bad example due to how simple it is and it seems obvious not to do that
//BUT its not always obvious! An example: http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js
//So if we're not careful things can go bad :(
//Just remember to keep track of your variables


//There are many ways to do this, one way is to prefix your variable name with its type!
var intOut = 5;
var stringOut = "HI";
var charOut = 'c';

//Or you can just make sure to remember.
//Having the ability to switch types is cool and actually really useful so we don't always have to create new variable
//If we want to output a string OR an integer

