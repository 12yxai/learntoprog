//Your first program!

//The first program almost everybody learns is the hello world program
//All this program does is outputs a simple line of text
//For example:
console.log("THIS IS OUTPUT");

//Note: The quotetations are needed to display the string

//Some other examples

console.log("HELLO");

console.log("GOOD BYE");

//Notice how each statement ends with a semicolon, this means the end of a command
//Basically it means: 
// Do this; Do this after; Do this last;

//Without the semicolons the computer gets confused!
//Do this do this after do this last ?? It doesnt know where the command ends!

//console.log(THIS DOESNT WORK);
//Javascript gets confused when you try to use text naturally, the reason being
//to javascript everything is a part of its own special syntax unless specified
//otherwise! Double quotes and single quotes are intended for this purpose.

console.log("YOU ARE NOT BATMAN!");
console.log("BUT I WILL BE");

