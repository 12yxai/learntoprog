//If Statements

//Going over boolean expressions you may have wondered, where and why are these
//useful? To that question I propose another question: What if you want your
//program to CHANGE its behavior based on certain information?

//Example: I only want to open the Dorm building door for people who
//live here. 

//How would you go about CHANGING the behavior of the door (from locked to unlocked)
//when the right person comes along?
console.log("SORRY THE DOOR IS LOCKED FOR EVERYONE");

//The answer we've been looking for is if statements.
//If statements are what are known as control abstractions: they control the flow
//of your code in the program you make.
console.log("----------------");
console.log("Normally");
console.log("code");
console.log("executes");
console.log("in");
console.log("order");

//Control abstractions allow us to change that, to make the code branch ie. opening the door ONLY
//if the person has the right ID or doing a repitive task several times. 

//A real world example of this is a Web Server. The serer has to be ready at all times to
//give someone the webpage they wanted. As a result, it is constantly listening
//for requests. Trying to program that with only sequential code would be impossible
//as the programmer would have to add new instructions constantly.

//If expressions work by evaluating a boolean expression, as we introduced in the last
//tutorial. If the expression is true, the program executes some code.
console.log("-----------");
if (true)
    console.log("THE EVALUATED STATEMENT IS TRUE");

//Note: I will get to the indentation in the next tutorial

//If has a partner ELSE, which is executed in the cases that the boolean expression evaluates to false
console.log("---------------------------");
if (false)
    console.log("This is not printed");
else
    console.log("This however, is printed");
    
//Lets combine our new knowledge with the boolean expressions:
var someNumber = 102;
if (someNumber < 100)
    console.log(someNumber + " is less than 100");
else
    console.log(someNumber + " is greater than or equal to 100");
//Change the value of someNumber around to see how it works!

//Using the dorm door example imagine we are given a student ID number:
var studentID = 107380444;
//Now for the sake of our example lets assume he is the ONLY person living in 
//the dorm, using techniques introduced later we can add more people!
if (studentID === 107380444)//Remember to use the triple equals
    console.log("Door is unlocked");
else
    console.log("Door is unlocked");

