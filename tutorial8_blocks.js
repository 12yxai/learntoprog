//Blocks

//If statements as we showed so far work great for single commands, but what if
//you wanted to apply them to multiple lines of code?

//This is the one of the uses of BLOCKS of code. Blocks of code are multiple lines
//of code wrapped into a single group. 
{
    console.log("This is in a block");
    console.log("This is also in a block");
}
console.log("This is not in a block of code");

//Blocks of code have two distinct features:
// 1. They are surrounded by brackes {}
// 2. Normally code inside them indented to convey that they are inside a block

//Why blocks are important for if statements:
var someValue = 45;
if (someValue === 55)
    console.log("I want this to appear ONLY IF someValue is equal to 55");//We don't want this to appear
    console.log("I want the same");//We don't want this to appear
    
//However the bottom line is always printed but using blocks of code we can tell
//the computer to treat a group of lines like a single group.

if (someValue === 45) {//Block starts here
    console.log("Hello");
    console.log("my friend");
}//End of block
console.log("James");
//Change the value of some value and watch what happens
//If someValue is not set to 45 then only James will be displayed

//Nesting blocks

//Blocks can be nested inside each other
//For example imagine if you wanted a person to be able to get money from an 
//ATM only with a correct Name and ID number

console.log("--------------------------------------");
var hiddenID = 3245;//These are the stored values
var hiddenName = "J Doe";//This is the stored name
var name = "J Doe";
var ID = 3245;
if (ID === hiddenID) {
    console.log("ID is correct!");
    if (name === hiddenName) {
        console.log("Sending out 20 dollars as Name is correct");
    }
}

