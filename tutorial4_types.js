//Types

//Variables are very powerful and store all sorts of expressions
var someInfo = "THIS CAN BE STORED";
var soCanThis = 55 + 0.0002;
var asCanThis = false;
console.log(someInfo);
console.log(soCanThis);
console.log(asCanThis);
//Types are a way of distinguishing between certain expressions
//For example
var a = 5;
var b = false;
//The addition we did in the previous tutorial inst possible with a and b
//To see what happens uncomment the next line
//a + b;

//Different types
//These are what are known as primitive types!
//They are called primitive because you can't break them down any further

//So what are the different types!

//There are numbers!
var int = 3;
int = 4;
int = 5;
console.log(int);


//There are decimals numbers(known as floats to programmers, more on that later)!
var double = 3.5;
double = 0.000005;
double = 0.9;
console.log(double);
//SideNote: Javascript technically only has floats. Other languages have integers,
//which are only whole numbers however, in javascript a number is a number.
//Just remember that 5 = 5.0

//There are characters!
var char = 'c';
char = 'f';
char = 'm';
console.log(char);

//There are booleans!
//Note: For those who don't know what booleans means they are simply values of 
//true or false
var bool = true;
bool = false;
bool = true;
console.log(bool);//The "" is necessary to make the result appear!

//There are strings!
//Strings are a funny type because they are primitive and aren't
//They are just a group of characters, but they are used so often with so many
//different programs that they are also used as primitives!
var string = "THIS IS A STRING";
string = "THIS IS ALSO A STRING";
string = "SO IS THIS";
console.log(string);

//




