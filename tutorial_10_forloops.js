//For Loops
//There are many cases where we might want to do a simple task multiple times
//For example refering to the first tutorial saying Hi five times

//Normally we would have to write
console.log("HI");
console.log("HI");
console.log("HI");
console.log("HI");
console.log*("HI");

//But why do that when we can say the following
console.log("---------------------------------------");
var aNumber = 0;
while (aNumber < 5) {
    console.log("HI");
    aNumber = aNumber + 1;
}

//The way a while loop works is simple, it will execute its contents UNTIL the 
//statement in parentathese is FALSE
while (false) {
    console.log("This won't be displayed");
}

//IF you were to execute the following code it would run until you turned it off by force

//while (true) {
//console.log("This is nonstop");
//}

//What if you want a number to be printed until it is greater then 50
var testVal = 0;//What should this be set to
var vals = -30;
while (vals < testVal) {
    console.log(vals);
    vals += 1;
    


